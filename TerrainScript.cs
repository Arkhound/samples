using UnityEngine;
using System.Collections;
using System.Linq;

public class TerrainScript : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		Refresh();
	}

	void Awake()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void Refresh()
	{
		StartCoroutine(_Refresh());
	}

	IEnumerator _Refresh()
	{
		var url = ""; //Put Wally's target IP here
		var qs = ""; //query, put lat-long inside here, maybe resolution

		print(url + "?" + qs);
		var req = new WWW(url + "?" + qs);
		yield return req; //req is the returned object (heightmap?)
		//mat.SetTexture("_MainTex", req.texture); //Use this if texture, probably not though

		//raw = req;

		//Generate all the terrain data according to the terrain, ours is pre-generated so we can use hard values
		TerrainData terrainData = new TerrainData();
		terrainData.size = new Vector3(64,32,64); //w, h, l
		terrainData.baseMapResolution = 128;
		terrainData.heightmapResolution = 129;
		terrainData.alphamapResolution = 128;
		terrainData.SetDetailResolution(128, 8);
		terrainData.name = name;

		//Load and texture the raw heightmap
		LoadTerrain("C:\\Git\\Unity\\ARLandNav\\Assets\\Models\\eustis-edited.raw", terrainData);
		SetTextures(terrainData);
		TextureTerrain(terrainData);

		//Create the actual terrain object and parent to grid
		GameObject terrain = (GameObject)Terrain.CreateTerrainGameObject(terrainData);
		terrain.name = name;
		terrain.transform.parent = GameObject.Find("Grid").transform;
		terrain.transform.position = new Vector3(0,0,0);
	}

	//Generate terrain point-by-point according the the raw heightmap
	void LoadTerrain(string filename, TerrainData tData)
	{
		int h = tData.heightmapHeight;
		int w = tData.heightmapWidth;
		float[,] data = new float[h,w];
		using(var file = System.IO.File.OpenRead(filename))
		using(var reader = new System.IO.BinaryReader(file))
		{
			for(int y = h-1; y >= 0; y--) //Reverse y because of how raw heightmaps are stored
				for(int x = 0; x < w; x++)
				{
					float v = (float)reader.ReadUInt16() / 0xFFFF;
					data[y, x] = v;
				}
		}
		tData.SetHeights(0, 0, data);
	}

	//Add textures to the list of "splats" to be used by the splatmapping
	void SetTextures(TerrainData tData)
	{
		SplatPrototype[] textures = new SplatPrototype[4];
		for(int i = 0; i < textures.Length; i++)
			textures[i] = new SplatPrototype();
		textures[0].texture = (Texture2D)Resources.Load("black");
		textures[1].texture = (Texture2D)Resources.Load("Cliff_DISP");
		textures[2].texture = (Texture2D)Resources.Load("ground_blue");
		textures[3].texture = (Texture2D)Resources.Load("ground_green");

		tData.splatPrototypes = textures;
	}

	//Texture individual height areas by using adjusted weight values depending on data
	void TextureTerrain(TerrainData tData)
	{
		float[,,] splatmapData = new float[tData.alphamapWidth, tData.alphamapHeight, tData.alphamapLayers];
		Debug.Log(tData.alphamapTextures.Length);
		Debug.Log(tData.alphamapHeight);
		Debug.Log(tData.alphamapWidth);

		float y_01, x_01, height, steepness, z;
		Vector3 normal;
		float[] splatWeights;

		for(int y = 0; y < tData.alphamapHeight; y++)
			for(int x = 0; x < tData.alphamapWidth; x++)
			{
				//Normalize (x,y) coords to 0-1 range
				y_01 = (float)y / (float)tData.alphamapHeight;
				x_01 = (float)x / (float)tData.alphamapWidth;

				//Sample height at location
				height = tData.GetHeight(Mathf.RoundToInt(y_01 * tData.heightmapHeight), Mathf.RoundToInt(x_01 * tData.heightmapWidth));

				//Calculate normal of terrain
				normal = tData.GetInterpolatedNormal(y_01, x_01);

				//Calculate steepness of terrain
				steepness = tData.GetSteepness(y_01, x_01);

				//Setup array to record mix of texture weights
				splatWeights = new float[tData.alphamapLayers];

				//CHANGE ACCORDING TO TEXTURE WEIGHT NEEDS

				//Texture[0] has constant confluence
				//print(splatWeights.Length);
				//print(terrainData.alphamapLayers);
				//print(steepness);
				splatWeights[0] = 0.5f;

				//Texture[1] is stronger on inclines
				splatWeights[1] = steepness/40f;

				//Texture[2] stronger on flatter terrain, steepness is unbounded so normalize
				splatWeights[2] = 1.0f - Mathf.Clamp01(steepness*steepness/(tData.heightmapHeight/25.0f));

				//Texture[3] increases with height but only on surfaces facing positive z axis
				splatWeights[3] = (height/20) * Mathf.Clamp01(normal.y);

				//sum of all texture weights must add to 1, calculate normalization factor from sum of weights
				z = splatWeights.Sum();

				//loop through each terrain texture
				for(int i = 0; i < tData.alphamapLayers; i++)
				{
					//normalize so that sum of all weights equals 1
					splatWeights[i] /= z;

					//assign this point to splatmap array
					splatmapData[x, y, i] = splatWeights[i];
				}
			}

		tData.SetAlphamaps(0, 0, splatmapData);
	}
}
