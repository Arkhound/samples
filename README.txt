Here are the code examples that I can publicly use for my portfolio.

Unfortunately, many of my professional contributions were not owned 
solely by me and I do not have the open use rights to all of my 
production code. For this reason, much of what I have available is 
neither current nor the best code that I've worked on.