﻿/*

    Steven Butkus:
    This file shows what each button is compromised of: an activity
    indicator showing if it is currently selected or not, a display
    name (or image in other projects), it's position on screen, the
    delegate action, and a visibility.

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using AAITD.Content;

namespace AAITD.States
{
    public class Button
    {
        //Delegate for event handling
        public delegate void PressEventHandler();

        //button fields
        public bool isActive = false;
        public string displayName = "";
        public PressEventHandler onButtonPress;
        public Vector2 position;
        public bool visible;

        public Button(bool _isActive, string _displayName, Vector2 pos, PressEventHandler _onButtonPress, bool _visible)
        {
            isActive = _isActive;
            displayName = _displayName;
            position = pos;
            onButtonPress = _onButtonPress;
            visible = _visible;
        }

        public void Press()
        {
            if (onButtonPress != null)
                onButtonPress.Invoke();
        }
    }

}
