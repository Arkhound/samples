﻿/*

    This is simply a use of the button functionality where the
    function "onButtonPress()" is called by a button in a list on
    line 49

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using AAITD.Content;

namespace AAITD.States
{
    public class ExpositionState : MenuState
    {
        Texture2D expositionText;
        Color fadeColor = Color.White;
        int inputDelay = 210;
        int ticker = 0;
        public ExpositionState(string path, List<Button> buttons)
            :base(buttons, null)
        {
            expositionText = ContentLoader.GetTexture(path);
        }

        public override void Update(float delta)
        {
            if (inputDelay > 0) inputDelay--;
            else
            {
                if (fadeColor.A > 0 && ticker == 0)
                {
                    fadeColor.A--;
                    fadeColor.B--;
                    fadeColor.G--;
                    fadeColor.R--;
                }
                ticker++;
                ticker %= 2;
                base.Update(delta);
            }
            if (fadeColor.A == 0)
                buttons[0].onButtonPress();
        }

        public override void Draw(SpriteBatch batch)
        {
            batch.Begin();
            batch.Draw(expositionText, Vector2.Zero, fadeColor);
            batch.End();
            base.Draw(batch);
        }
    }
}
