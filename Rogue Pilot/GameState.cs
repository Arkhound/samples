﻿/*

    Steven Butkus:
    This file shows the actual composition of the state manager. We
    use states to represent game, menu, introduction, and other
    transitional periods between gameplay. By utilizing the Button
    class, we give each state it's necessary functionality by
    bolstering it with actionable lambda operations so that there is
    less redundant code and adding any additional content is limited
    solely by the extra function required. An example of these
    in use is visible in the ExpositionState.cs class.

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using AAITD.States;
using Microsoft.Xna.Framework;
using AAITD.Particles;
using AAITD.Ships;

namespace AAITD
{
    public interface GameState
    {
        void Update(float delta);
        void Draw(SpriteBatch batch);
    }

    public static class GlobalStateManager
    {
        private static Stack<GameState> states = new Stack<GameState>();

        //Loads all states, their respective buttons, and each buttons' functionality using delegates for button commands.
        public static void Load()
        {
            introState = new ExpositionState("IntroTest", new List<Button>()
            {
                new Button(false, "Start Game", new Vector2(585, 460), () => Push(new MainState()), false)
            });
            titleState = new MenuState(new List<Button>()
            {
                new Button(true, "Start Game", new Vector2(585, 460), () => Push(introState), true),
                new Button(false, "Exit", new Vector2(615, 520), () => ExitGame(), true)
            }, Content.ContentLoader.GetTexture("titlescreen"));
            pauseState = new EndState(new List<Button>()
            {
                new Button(true, "ResumeButton", new Vector2(608, 450),() => Pop(), true),
                new Button(false, "QuitButton", new Vector2(608, 500),() => PopToTitle(), true)
            }, null, Color.Gray);
            gameOverState = new EndState(new List<Button>()
            {
                new Button(false, "Return to Title Screen", new Vector2(608, 450),() => PopToTitle(), false)
            }, null, Color.White);
            transitionState = new EndState(new List<Button>()
            {
                new Button(false, "Next Level", new Vector2(608, 450),() => Pop(), false)
            }, null, Color.White);
        }

        public static void Push(GameState state)
        {
            states.Push(state);
        }
        public static void Pop()
        {
            states.Pop();
        }
        public static void PopToTitle()
        {
            for(int i = states.Count; i != 1; i--)
                states.Pop();
            ClearLists();
        }
        public static GameState Peek()
        {
            return states.Peek();
        }
        public static void ExitGame()
        {
            System.Environment.Exit(0);
        }

        public static void Update(float delta)
        {
            Peek().Update(delta);
        }
        public static void Draw(SpriteBatch batch)
        {
            Peek().Draw(batch);
        }

        public static void ClearLists()
        {
            foreach(Ship s in MainState.Ships)
            {
                MainState.world.enabledObjects.Remove(s);
            }
            foreach (Projectile p in MainState.Projectiles)
            {
                MainState.world.enabledObjects.Remove(p);
            }
            foreach (Module d in MainState.Debris)
            {
                MainState.world.enabledObjects.Remove(d);
            }
            MainState.Ships.Clear();
            MainState.Projectiles.Clear();
            MainState.Debris.Clear();
            LevelFactory.Reset();
        }
    }
}
