/*
    Steven Butkus:
    This file was the main game loop and it's efforts were a
    combination between three other group members. The portion of the
    file that I would like to highlight is lines 80-82 where I
    utilized the state manager to load in all the transitional logic
    and push the intial state. This is not the full program but it
    is the first important file in understanding the structure of
    how I used delegates to push and pull states by creating a simple
    set of buttons with deep functionality.

    This project was created in under 48 hours during the 2014 Global
    Game Jam. My role was strictly as a programmer. My teammates
    included Adrien Young, Kimberly Shannon, and Mark Gerow.

*/

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using AAITD.Objects;
using AAITD.Physics;
using AAITD.Components;
using AAITD.Content;
using AAITD.States;
using AAITD.Ships;
using AAITD.Graphics;

namespace AAITD
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public static SpriteFont Debug;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferWidth = 1280;
            this.IsMouseVisible = true;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            ContentLoader.Initialize(Services);
            Debug = Content.Load<SpriteFont>("DebugFont");

            //Load in state content
            GlobalStateManager.Load();
            GlobalStateManager.Push(new MenuState(GlobalStateManager.titleState.buttons, ContentLoader.GetTexture("titlescreen")));

            Particles.ParticleManager.Initialize();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }


        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            float elapsed = 0.0166666667f;
            Input.Update();
            GlobalStateManager.Update(elapsed);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            GlobalStateManager.Draw(spriteBatch);

            base.Draw(gameTime);
        }
    }
}
