using UnityEngine;
using System.Collections;
using System;

public class WMM
{
    #region WMM Input data
    private string[] input =
    {   "2015.0", "WMM-2015", "12/15/2014",
        "1", "0", "-29438.5", "0.0", "10.7", "0.0",
        "1", "1", "-1501.1", "4796.2", "17.9", "-26.8",
        "2", "0", "-2445.3", "0.0", "-8.6", "0.0",
        "2", "1", "3012.5", "-2845.6", "-3.3", "-27.1",
        "2", "2", "1676.6", "-642.0", "2.4", "-13.3",
        "3", "0", "1351.1", "0.0", "3.1", "0.0",
        "3", "1", "-2352.3", "-115.3", "-6.2", "8.4",
        "3", "2", "1225.6", "245.0", "-0.4", "-0.4",
        "3", "3", "581.9", "-538.3", "-10.4", "2.3",
        "4", "0", "907.2", "0.0", "-0.4", "0.0",
        "4", "1", "813.7", "283.4", "0.8", "-0.6",
        "4", "2", "120.3", "-188.6", "-9.2", "5.3",
        "4", "3", "-335.0", "180.9", "4.0", "3.0",
        "4", "4", "70.3", "-329.5", "-4.2", "-5.3",
        "5", "0", "-232.6", "0.0", "-0.2", "0.0",
        "5", "1", "360.1", "47.4", "0.1", "0.4",
        "5", "2", "192.4", "196.9", "-1.4", "1.6",
        "5", "3", "-141.0", "-119.4", "0.0", "-1.1",
        "5", "4", "-157.4", "16.1", "1.3", "3.3",
        "5", "5", "4.3", "100.1", "3.8", "0.1",
        "6", "0", "69.5", "0.0", "-0.5", "0.0",
        "6", "1", "67.4", "-20.7", "-0.2", "0.0",
        "6", "2", "72.8", "33.2", "-0.6", "-2.2",
        "6", "3", "-129.8", "58.8", "2.4", "-0.7",
        "6", "4", "-29.0", "-66.5", "-1.1", "0.1",
        "6", "5", "13.2", "7.3", "0.3", "1.0",
        "6", "6", "-70.9", "62.5", "1.5", "1.3",
        "7", "0", "81.6", "0.0", "0.2", "0.0",
        "7", "1", "-76.1", "-54.1", "-0.2", "0.7",
        "7", "2", "-6.8", "-19.4", "-0.4", "0.5",
        "7", "3", "51.9", "5.6", "1.3", "-0.2",
        "7", "4", "15.0", "24.4", "0.2", "-0.1",
        "7", "5", "9.3", "3.3", "-0.4", "-0.7",
        "7", "6", "-2.8", "-27.5", "-0.9", "0.1",
        "7", "7", "6.7", "-2.3", "0.3", "0.1",
        "8", "0", "24.0", "0.0", "0.0", "0.0",
        "8", "1", "8.6", "10.2", "0.1", "-0.3",
        "8", "2", "-16.9", "-18.1", "-0.5", "0.3",
        "8", "3", "-3.2", "13.2", "0.5", "0.3",
        "8", "4", "-20.6", "-14.6", "-0.2", "0.6",
        "8", "5", "13.3", "16.2", "0.4", "-0.1",
        "8", "6", "11.7", "5.7", "0.2", "-0.2",
        "8", "7", "-16.0", "-9.1", "-0.4", "0.3",
        "8", "8", "-2.0", "2.2", "0.3", "0.0",
        "9", "0", "5.4", "0.0", "0.0", "0.0",
        "9", "1", "8.8", "-21.6", "-0.1", "-0.2",
        "9", "2", "3.1", "10.8", "-0.1", "-0.1",
        "9", "3", "-3.1", "11.7", "0.4", "-0.2",
        "9", "4", "0.6", "-6.8", "-0.5", "0.1",
        "9", "5", "-13.3", "-6.9", "-0.2", "0.1",
        "9", "6", "-0.1", "7.8", "0.1", "0.0",
        "9", "7", "8.7", "1.0", "0.0", "-0.2",
        "9", "8", "-9.1", "-3.9", "-0.2", "0.4",
        "9", "9", "-10.5", "8.5", "-0.1", "0.3",
        "10", "0", "-1.9", "0.0", "0.0", "0.0",
        "10", "1", "-6.5", "3.3", "0.0", "0.1",
        "10", "2", "0.2", "-0.3", "-0.1", "-0.1",
        "10", "3", "0.6", "4.6", "0.3", "0.0",
        "10", "4", "-0.6", "4.4", "-0.1", "0.0",
        "10", "5", "1.7", "-7.9", "-0.1", "-0.2",
        "10", "6", "-0.7", "-0.6", "-0.1", "0.1",
        "10", "7", "2.1", "-4.1", "0.0", "-0.1",
        "10", "8", "2.3", "-2.8", "-0.2", "-0.2",
        "10", "9", "-1.8", "-1.1", "-0.1", "0.1",
        "10", "10", "-3.6", "-8.7", "-0.2", "-0.1",
        "11", "0", "3.1", "0.0", "0.0", "0.0",
        "11", "1", "-1.5", "-0.1", "0.0", "0.0",
        "11", "2", "-2.3", "2.1", "-0.1", "0.1",
        "11", "3", "2.1", "-0.7", "0.1", "0.0",
        "11", "4", "-0.9", "-1.1", "0.0", "0.1",
        "11", "5", "0.6", "0.7", "0.0", "0.0",
        "11", "6", "-0.7", "-0.2", "0.0", "0.0",
        "11", "7", "0.2", "-2.1", "0.0", "0.1",
        "11", "8", "1.7", "-1.5", "0.0", "0.0",
        "11", "9", "-0.2", "-2.5", "0.0", "-0.1",
        "11", "10", "0.4", "-2.0", "-0.1", "0.0",
        "11", "11", "3.5", "-2.3", "-0.1", "-0.1",
        "12", "0", "-2.0", "0.0", "0.1", "0.0",
        "12", "1", "-0.3", "-1.0", "0.0", "0.0",
        "12", "2", "0.4", "0.5", "0.0", "0.0",
        "12", "3", "1.3", "1.8", "0.1", "-0.1",
        "12", "4", "-0.9", "-2.2", "-0.1", "0.0",
        "12", "5", "0.9", "0.3", "0.0", "0.0",
        "12", "6", "0.1", "0.7", "0.1", "0.0",
        "12", "7", "0.5", "-0.1", "0.0", "0.0",
        "12", "8", "-0.4", "0.3", "0.0", "0.0",
        "12", "9", "-0.4", "0.2", "0.0", "0.0",
        "12", "10", "0.2", "-0.9", "0.0", "0.0",
        "12", "11", "-0.9", "-0.2", "0.0", "0.0",
        "12", "12", "0.0", "0.7", "0.0", "0.0"
    };
    #endregion

    double alt = 0;                     //Geodetic altitude in km
    double glat = 0;                    //Geodetic latitude in degrees
    double glong = 0;                   //Geodetci longitude in degrees
    double time = 0;                    //Time in decimal years
    double dec = 0;                     //Geomagnetic declination in degrees (east +, west -)
    double dip = 0;                     //Geomagnetic inclination in degrees (down +, up -)
    double ti = 0;                      //Geomagnetic total intensity in nano Teslas
    int maxdeg = 12;                    //Maximum degrees of spherical harmonic model
    int maxord;                         //Maximum order of spherical harmonic model
    double defaultDate = 2017.5;        //Default date if not specified
    double defaultAlt = 0;              //Default altitude if not specified

    double[,] c = new double[13, 13];   //Gauss coefficients of main geomagnetic model (nt)
    double[,] cd = new double[13, 13];  //Gauss coefficients of secular geomagnetic model (nt/yr)
    double[,] tc = new double[13, 13];  //Time adjusted geomagnetic gauss coefficients (nt)
    double[,] dp = new double[13, 13];  //Theta derivative of p(n,m) (unnormalized)

    double[] snorm = new double[169];   //Schmidt normalizaiton factors
    double[] sp = new double[13];       //Sine of (m*spherical longitude)
    double[] cp = new double[13];       //Cosine of (m*spherical longitude)
    double[] fn = new double[13];
    double[] fm = new double[13];

    //Associated Legendre polynomials for m=1 (unnormalized)
    double[] pp = new double[13];
    double[,] k = new double[13, 13];

    double otime, oalt, olat, olon;     //Old storage variables if inputs don't change;

    double epoch;                       //Date in years for the start of valid time of the fit coefficients

    double bx, by, bz, bh;              //N, E, Vert, and Hor field intensities
    double re, a2, b2, c2, a4, b4, c4;
    double r, d, ca, sa, ct, st;

    public WMM()
    {
        initModel();
    }

    void initModel()
    {
        glat = 0;
        glong = 0;

        maxord = maxdeg;
        sp[0] = 0.0;
        cp[0] = snorm[0] = pp[0] = 1.0;
        dp[0,0] = 0.0;

        double a = 6378.137;            //Semi-major axis of the WGS-84 ellipsoid in km
        double b = 6356.7523142;        //Semi-minor axis of the WGS-84 ellipsoid in km
        re = 6371.2;                    //mean radius of IAU-66 ellipsoid in km
        a2 = a * a;
        b2 = b * b;
        c2 = a2 - b2;
        a4 = a2 * a2;
        b4 = b2 * b2;
        c4 = a4 - b4;

        setCoeff();

        snorm[0] = 1.0;
        for(int n = 1; n <= maxord; n++)
        {

            snorm[n] = snorm[n - 1] * (2 * n - 1) / n;
            int j = 2;

            for(int m = 0, D1 = 1, D2 = (n - m + D1) / D1; D2 > 0; D2--, m += D1)
            {
                k[m, n] = (double)(((n - 1) * (n - 1)) - (m * m)) / (double)((2 * n - 1) * (2 * n - 3));
                if(m > 0)
                {
                    double flnmj = ((n - m + 1) * j) / (double)(n + m);
                    snorm[n + m * 13] = snorm[n + (m - 1) * 13] * Math.Sqrt(flnmj);
                    j = 1;
                    c[n, m - 1] = snorm[n + m * 13] * c[n, m - 1];
                    cd[n, m - 1] = snorm[n + m * 13] * cd[n, m - 1];
                }
                c[m, n] = snorm[n + m * 13] * c[m, n];
                cd[m, n] = snorm[n + m * 13] * cd[m, n];
            }

            fn[n] = (n + 1);
            fm[n] = n;

        }

        k[1, 1] = 0.0;

        otime = oalt = olat = olon = -1000.0;
    }

    void calcGeoMag(double flat, double flon, double year, double altitude)
    {
        glat = flat;
        glong = flon;
        alt = altitude;

        time = year;

        double dt = time - epoch;

        double pi = Math.PI;
        double dtr = (pi / 180.0);
        double rlon = glong * dtr;
        double rlat = glat * dtr;
        double srlon = Math.Sin(rlon);
        double srlat = Math.Sin(rlat);
        double crlon = Math.Cos(rlon);
        double crlat = Math.Cos(rlat);
        double srlat2 = srlat * srlat;
        double crlat2 = crlat * crlat;
        sp[1] = srlon;
        cp[1] = crlon;

        if(alt != oalt || glat != olat)
        {
            double q = Math.Sqrt(a2 - c2 * srlat2);
            double q1 = alt * q;
            double q2 = ((q1 + a2) / (q1 + b2)) * ((q1 + a2) / (q1 + b2));
            ct = srlat / Math.Sqrt(q2 * crlat2 + srlat2);
            st = Math.Sqrt(1.0 - (ct * ct));
            double r2 = ((alt * alt) + 2.0 * q1 + (a4 - c4 * srlat2) / (q * q));
            r = Math.Sqrt(r2);
            d = Math.Sqrt(a2 * crlat2 + b2 * srlat2);
            ca = (alt + d) / r;
            sa = c2 * crlat * srlat / (r * d);
        }
        if(glong != olon)
        {
            for(int m = 2; m <= maxord; m++)
            {
                sp[m] = sp[1] * cp[m - 1] + cp[1] * sp[m - 1];
                cp[m] = cp[1] * cp[m - 1] - sp[1] * sp[m - 1];
            }
        }
        double aor = re / r;
        double ar = aor * aor;
        double br = 0, bt = 0, bp = 0, bpp = 0;

        for(int n = 1; n <= maxord; n++)
        {
            ar = ar * aor;
            for(int m = 0, D3 = 1, D4 = (n + m + D3) / D3; D4 > 0; D4--, m += D3)
            {

                //COMPUTE UNNORMALIZED ASSOCIATED LEGENDRE POLYNOMIALS
                //AND DERIVATIVES VIA RECURSION RELATIONS
                if(alt != oalt || glat != olat)
                {
                    if(n == m)
                    {
                        snorm[n + m * 13] = st * snorm[n - 1 + (m - 1) * 13];
                        dp[m, n] = st * dp[m - 1, n - 1] + ct * snorm[n - 1 + (m - 1) * 13];
                    }
                    if(n == 1 && m == 0)
                    {
                        snorm[n + m * 13] = ct * snorm[n - 1 + m * 13];
                        dp[m, n] = ct * dp[m, n - 1] - st * snorm[n - 1 + m * 13];
                    }
                    if(n > 1 && n != m)
                    {
                        if(m > n - 2)
                            snorm[n - 2 + m * 13] = 0.0;
                        if(m > n - 2)
                            dp[m, n - 2] = 0.0;
                        snorm[n + m * 13] = ct * snorm[n - 1 + m * 13] - k[m, n] * snorm[n - 2 + m * 13];
                        dp[m, n] = ct * dp[m, n - 1] - st * snorm[n - 1 + m * 13] - k[m, n] * dp[m, n - 2];
                    }
                }

                //TIME ADJUST THE GAUSS COEFFICIENTS
                if(time != otime)
                {
                    tc[m, n] = c[m, n] + dt * cd[m, n];

                    if(m != 0)
                        tc[n, m - 1] = c[n, m - 1] + dt * cd[n, m - 1];
                }

                //ACCUMULATE TERMS OF THE SPHERICAL HARMONIC EXPANSIONS
                double temp1, temp2;
                double par = ar * snorm[n + m * 13];
                if(m == 0)
                {
                    temp1 = tc[m, n] * cp[m];
                    temp2 = tc[m, n] * sp[m];
                }
                else
                {
                    temp1 = tc[m, n] * cp[m] + tc[n, m - 1] * sp[m];
                    temp2 = tc[m, n] * sp[m] - tc[n, m - 1] * cp[m];
                }

                bt = bt - ar * temp1 * dp[m, n];
                bp += (fm[m] * temp2 * par);
                br += (fn[n] * temp1 * par);

                //SPECIAL CASE:  NORTH/SOUTH GEOGRAPHIC POLES
                if(st == 0.0 && m == 1)
                {
                    if(n == 1)
                        pp[n] = pp[n - 1];
                    else
                        pp[n] = ct * pp[n - 1] - k[m, n] * pp[n - 2];
                    double parp = ar * pp[n];
                    bpp += (fm[m] * temp2 * parp);
                }

            }

        }


        if(st == 0.0)
            bp = bpp;
        else
            bp /= st;

        //ROTATE MAGNETIC VECTOR COMPONENTS FROM SPHERICAL TO
        //GEODETIC COORDINATES
        // bx must be the east-west field component
        // by must be the north-south field component
        // bz must be the vertical field component.
        bx = -bt * ca - br * sa;
        by = bp;
        bz = bt * sa - br * ca;

        //COMPUTE DECLINATION (DEC), INCLINATION (DIP) AND
        //TOTAL INTENSITY (TI)

        bh = Math.Sqrt((bx * bx) + (by * by));
        ti = Math.Sqrt((bh * bh) + (bz * bz));

        //Calculate the declination.
        dec = (Math.Atan2(by, bx) / dtr);
        dip = (Math.Atan2(bz, bh) / dtr);

        otime = time;
        oalt = alt;
        olat = glat;
        olon = glong;
    }

    public double getDeclination(double dlat, double dlong)
    {
        calcGeoMag(dlat, dlong, defaultDate, defaultAlt);
        return dec;
    }

    public double getDeclination(double dlat, double dlong, double year, double altitude)
    {
        calcGeoMag(dlat, dlong, year, altitude);
        return dec;
    }

    //Sets the input data to the internal coefficients
    void setCoeff()
    {
        c[0,0] = 0.0;
        cd[0, 0] = 0.0;

        epoch = Double.Parse(input[0]);
        defaultDate = epoch + 2.5;

        for(int i = 3; i < input.Length; i = i + 6)
        {
            int n = Int32.Parse(input[i]);
            int m = Int32.Parse(input[i+1]);
            double gnm = Double.Parse(input[i+2]);
            double hnm = Double.Parse(input[i+3]);
            double dgnm = Double.Parse(input[i+4]);
            double dhnm = Double.Parse(input[i+5]);

            if(m <= n)
            {
                c[m, n] = gnm;
                cd[m, n] = dgnm;

                if(m != 0)
                {
                    c[n, m - 1] = hnm;
                    cd[n, m - 1] = dhnm;
                }
            }
        }
    }
}
