using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DotNetCoords;
public class Positioning : MonoBehaviour 
{
    public bool initialized;
    public GameController gameInfo;
    public Vector2 startLatLong;
    public Vector2 endLatLong;
    public Vector3 startPos;
    public Vector3 endPos;
    public float speed;
    public Text latlongpos;
    public LocationInfo currentGPSPosition;
    string gpsString;
    public LatLng LatLongInput;

    Vector3 touchStart;
    Vector3 cameraStartRot;
    Vector3 startoffset;

	// Use this for initialization
	void Start () 
    {
        gameInfo = Object.FindObjectOfType<GameController>();
        Input.location.Start();
//        Persistance.PlayerStartPos = new LatLng(37.158547, -76.583214);
//        Persistance.PlayerCurrentPos = new LatLng(37.158547, -76.583214);

        Gyroscope gyro = Input.gyro;
        gyro.enabled = true;

//        Input.gyro.enabled = true;
        Debug.Log(SystemInfo.supportsGyroscope);
        Debug.Log(SystemInfo.deviceModel);
        startoffset = new Vector3(Input.gyro.attitude.x, Input.gyro.attitude.y, 0);

//        if(Application.isEditor)
            RandomizeFlag(10, 100, 25);


//        Debug.Log(Input.gyro.rotationRateUnbiased);
//        gameInfo.output.text = Input.gyro.rotationRateUnbiased.ToString();

//        InvokeRepeating("walk", 0, 1.0f);
	}
	void Update ()
    {
//        Compass compass = new Compass();
//        Debug.Log(Input.compass.magneticHeading);
//        if (Input.GetMouseButtonUp(0))
//        {
//            touchStart = Vector3.zero;
//            cameraStartRot = Vector3.zero;
//        }
//        if (Input.GetMouseButton(0))
//        {
//            if (touchStart == Vector3.zero)
//            {
//                touchStart = Input.mousePosition;
//                cameraStartRot = transform.eulerAngles;
//            }
//
//            Vector3 rot = (touchStart - Input.mousePosition);
//            float step = speed * Time.deltaTime;
//            transform.eulerAngles = Vector3.MoveTowards(transform.eulerAngles, new Vector3(rot.y + cameraStartRot.x, -rot.x + cameraStartRot.y, 0), step);
//        }
//        if (Input.GetKey(KeyCode.W))
//            Persistance.PlayerCurrentPos = new LatLng(Persistance.PlayerCurrentPos.Latitude + .00001f, Persistance.PlayerCurrentPos.Longitude);
//        if (Input.GetKey(KeyCode.S))
//            Persistance.PlayerCurrentPos = new LatLng(Persistance.PlayerCurrentPos.Latitude - .00001f, Persistance.PlayerCurrentPos.Longitude);
//        if (Input.GetKey(KeyCode.A))
//            Persistance.PlayerCurrentPos = new LatLng(Persistance.PlayerCurrentPos.Latitude , Persistance.PlayerCurrentPos.Longitude - .00001f);
//        if (Input.GetKey(KeyCode.D))
//            Persistance.PlayerCurrentPos = new LatLng(Persistance.PlayerCurrentPos.Latitude, Persistance.PlayerCurrentPos.Longitude + .00001f);
        if (Input.location.status == LocationServiceStatus.Running)
        {
            if (!initialized)
            {
                Persistance.PlayerStartPos = new LatLng(Input.location.lastData.latitude, Input.location.lastData.longitude);
                Persistance.PlayerCurrentPos = new LatLng(Input.location.lastData.latitude, Input.location.lastData.longitude);
                RandomizeFlag(10, 100, 25);
                initialized = true;
            }
            Persistance.PlayerCurrentPos = new LatLng(Input.location.lastData.latitude, Input.location.lastData.longitude);
//            transform.position = new Vector3(((float)(Persistance.PlayerCurrentPos.Latitude - Persistance.PlayerStartPos.Latitude) * 111100.0f), 0, ((float)(Persistance.PlayerCurrentPos.Longitude - Persistance.PlayerStartPos.Longitude) * 111100.0f));
            gameInfo.output.text = Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " pos " + transform.position + " rot " + transform.eulerAngles;
        }
        else
        {
            gameInfo.output.text = Input.location.status.ToString() + "rot "+transform.eulerAngles;
//            gameInfo.output.text = Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " pos " + transform.position + " rot " + transform.eulerAngles;
        }
        Debug.Log(Input.gyro.attitude);
//        Debug.Log(Input.gyro.rotationRateUnbiased);
//        transform.Rotate(new Vector3(Input.gyro.attitude.x * 90.0f, 0,0));
//        transform.Rotate(new Vector3(-Input.gyro.rotationRateUnbiased.x, -Input.gyro.rotationRateUnbiased.y,0));
//        Debug.Log(Input.gyro.rotationRateUnbiased);
//        Debug.Log(Input.acceleration);
//        transform.rotation = Input.gyro.attitude;
//        transform.eulerAngles = new Vector3(Input.gyro.attitude.x * 90.0f,transform.eulerAngles.y,0);
//        transform.eulerAngles = new Vector3(transform.eulerAngles.x,transform.eulerAngles.y,0);
//        latlongpos.text = "Distance from start " + Persistance.PlayerStartPos.Distance(Persistance.PlayerCurrentPos)
//            + " Cartesian Conversion " + (Persistance.PlayerCurrentPos.Latitude - Persistance.PlayerStartPos.Latitude)
//            + " " +(Persistance.PlayerCurrentPos.Longitude - Persistance.PlayerStartPos.Longitude)
//            ;
        //Debug.Log(Persistance.PlayerStartPos.Latitude + " " + Persistance.PlayerCurrentPos.Latitude + (Persistance.PlayerStartPos.Latitude - Persistance.PlayerCurrentPos.Latitude));
	
    }

//    public void walk()
//    {
//        Persistance.PlayerCurrentPos = new LatLng(Persistance.PlayerCurrentPos.Latitude + .00001f, Persistance.PlayerCurrentPos.Longitude + .00001f);
//    }

    MGRSRef latlongtomgrs(double lat, double lng)
    {
        LatLongInput = new LatLng ( lat, lng, 0);
        MGRSRef mgrsRerf = LatLongInput.ToMGRSRef ();
        return mgrsRerf;
    }
    double MGRSDist(MGRSRef a, MGRSRef b)
    {
        LatLng ll = a.ToLatLng();

        return ll.Distance(b.ToLatLng());  //Outputs in km's
    }
    double llDist(Vector2 a, Vector2 b)
    {
        LatLng ll = new LatLng(a.x, a.y);
        return ll.Distance(new LatLng(b.x,b.y));
    }
    void RandomizeFlag(int numFlags, int randRadius, int minRad)
    {
        for (int i = 0; i < numFlags; i++)
        {
            GameObject go = Instantiate(gameInfo.FlagPrefab, new Vector3(RandomRange(randRadius, minRad), 0, RandomRange(randRadius, minRad)), Quaternion.Euler(new Vector3(0,0,0))) as GameObject;
//            Debug.Log("Distance " + Vector3.Distance(go.transform.position, transform.position), go);
//            GameObject go = Instantiate(gameInfo.FlagPrefab, new Vector3(Random.Range(-randRadius, randRadius), 0, Random.Range(-randRadius, randRadius)), Quaternion.Euler(new Vector3(0,0,0))) as GameObject;

            if (go.GetComponentInChildren<signPostText>())
                go.GetComponentInChildren<signPostText>().MarkerValue = i;
            else
                go.transform.eulerAngles = new Vector3(0, Random.Range(0, 359), 0);
        }
    }

    float RandomRange(int MaxRange, int MinRange)
    {
        float rand = Random.Range(MinRange, MaxRange);
        rand = Random.Range(0, 2) == 0 ? rand * -1 : rand;
        return rand;
    }
}

